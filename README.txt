Description:
This module provides an input format filter to allow you to change base urls of images embedded in content. 
Please note this module will only changes urls of images that have the same base url of the site or that are linked relatively.

The original reason I decided to develop this module was so that hard coded images in posts could be rewritten for use with a cdn.

Installation:
1. Enable Image Url Filter 
2. Go to admin/settings/filters
3. Choose which filter you want to use this on and click configure
4. Check the checkbox next to 'Image Url Filter' and save
5. Next at the top of the page you are on look for the 'Configure' tab and click that
6. Enter in the full path to the url you wish to rewrite to ie http://cloudfront.amaxon.com